FROM postgres:11-alpine

LABEL maintainer="nacho@gesplan.es"

ENV PG_CRON_VERSION="1.2.0" \
	POSTGRES_WORK_MEM="1GB"

RUN apk add --no-cache --virtual \
		.build-deps \
		build-base=~0.5-r1 \
		cmake=~3.15 \
		ca-certificates=~20191127 \
		openssl=~1.1 \
		clang=~9.0 \
		llvm9=~9.0 \
		tar=~1 

# hadolint ignore=DL3003
RUN wget -O pg_cron.tgz https://github.com/citusdata/pg_cron/archive/v${PG_CRON_VERSION}.tar.gz && \
	tar xvzf pg_cron.tgz && \
	cd pg_cron-${PG_CRON_VERSION} && \
	sed -i.bak -e 's/-Werror//g' Makefile && \
	sed -i.bak -e 's/-Wno-implicit-fallthrough//g' Makefile && \
	make && \
	make install && \
	cd / && \
	rm -rf pg_cron.tgz pg_cron-* && \
	echo "shared_preload_libraries='pg_cron'" >> /usr/local/share/postgresql/postgresql.conf.sample && \
	echo "checkpoint_timeout = 30min" >> /usr/local/share/postgresql/postgresql.conf.sample && \
	echo "max_wal_size = 2GB" >> /usr/local/share/postgresql/postgresql.conf.sample && \
	mv /usr/local/bin/docker-entrypoint.sh /usr/local/bin/docker-entrypoint-origin.sh

COPY rootfs /
